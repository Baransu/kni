// @flow
import React from 'react';
import Header from '../components/Header';
import Sections from '../components/Sections';
import Description from '../components/Description';
import Footer from '../components/Footer';
import content from '../content';

const Index = ({ params }) => {
  const { head, description, sections, contact } = content;

  return (
    <div className="index-page">

      <Header {...head} />
      <Description description={description} />
      <Sections sections={sections} />
      <Footer contact={contact} />

    </div>
  );
};

export default Index;
