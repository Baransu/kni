// @flow
import React from 'react';

import Navigation from '../components/Navigation';

const Layout = ({ children }) => {
  return (
    <div className="container-full">

      <Navigation items={[]} />

      {children}

    </div>
  );
};

export default Layout;
