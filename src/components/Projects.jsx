// @flow
import React, { Component } from 'react'

import { Link, Element } from 'react-scroll';

export default class Projects extends Component {
  constructor(props) {
    super(props);
    const { projects } = props;
    this.state = {
      projects: projects.map((p, index) => ({...p, id: index})),
      current: 0
    }
  }

  onProjectChange(id) {
    this.setState({ current: id });
  }

  render() {
    const current = this.state.projects.find(p => p.id === this.state.current);

    const Project = ({ id, name, short, small, next, onClick }) => {
      const selected = this.state.current === id;

      const currentProject = window.innerWidth > 768 ? 'noop' : 'current-project';

      return (
        <Link to={`${next ? 'contact': currentProject}`} spy={true} smooth={true} offset={0} duration={1000} style={{color: 'black'}}>
          <div className="col-xs-12 col-md-3 col-sm-6 project-small m-b-20" onClick={() => { if(!next) onClick(id) }}>
            <img
              src={small}
              alt={name}
              className={`img-responsive m-b-10 ${selected ? 'selected' : ''}`}
            />

            <h4>{name}</h4>
            <span>{short}</span>
            <div className="divider col-xs-12 hidden-md hidden-lg m-t-10"></div>
          </div>
        </Link>
      );
    }

    const CurrentProject = () => (
      <Element name="current-project" className="col-xs-12 m-b-20">

        <div className="col-xs-12 col-md-6">

          <h4 className="col-xs-12 p-head">{current.name}</h4>
          <span className="p-stack col-xs-12">{current.stack}</span>
          <div className="divider col-xs-12 m-t-10"></div>
          <span className="col-xs-12 m-t-20 m-b-30 p-description">{current.description}</span>
          <div className="col-xs-12 hidden-md hidden-lg m-b-30">
            <img src={current.large} alt={current.name} className="img-responsive project-img"/>
          </div>

          <Link to="contact" spy={true} smooth={true} offset={0} duration={1000}>
            <button type="button" className="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 btn btn-success hire-button m-b-20">
              hire us
            </button>
          </Link>

        </div>

        <div className="col-md-6 col-xs-12 hidden-sm hidden-xs">
          <img src={current.large} alt={current.name} className="img-responsive" />
        </div>

      </Element>
    );

    return (
      <div className="container-full section text-center projects-section">
        <div className="container">
          <h3 className="col-xs-12 m-t-40 m-b-30 text-center">Projects</h3>

          <div className="col-xs-12 m-t-10 m-b-40">
            {this.state.projects.map((p, index) =>
              <Project key={index} {...p} onClick={this.onProjectChange.bind(this)}/>
             )}
          </div>

          <CurrentProject />

        </div>
      </div>
    );
  }
}
