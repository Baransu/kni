// @flow
import React, { Component } from 'react';
import { Element, Link, scroller } from 'react-scroll';

export default class Team extends Component {
  constructor(props) {
    super(props);
    const { team, current } = props;

    const member = team.find(t => t.name.toLowerCase().includes(current));

    const currentID = member ? team.indexOf(member) : 1;

    this.state = {
      team: team.map((t, index) => ({...t, id: index})),
      current: currentID,
      scroll: member !== undefined
    };
  }

  componentDidMount() {
    const { scroll } = this.state;
    if(scroll) {
      scroller.scrollTo("team", {
        spy: true,
        delay: 1200,
        smooth: true,
        offset: 0,
        duration: 1000
      });
    }
  }

  onSelectMember(id) {
    this.setState({
      current: id
    });
  }

  render() {

    const current = this.state.team.find(t => t.id === this.state.current);

    return (
      <div className="container-full section team-section">
        {/* <Element name="team">
            <div className="container">
            <h3 className="col-xs-12 m-t-40 m-b-30 text-center">Team</h3>
            <div className="divider col-xs-6 col-xs-offset-3 m-b-30"></div>
            {this.state.team.map((t, index) => (
            <div key={index} className="col-xs-4 text-center team-img"
            onClick={() => this.onSelectMember(t.id)}>
            <img
            src={t.large}
            alt={t.name}
            className={`m-b-10 img-circle ${t.id === current.id ? '' : 'not-' }selected`} />
            <div className={`col-xs-12 team-name text-center m-t-40 m-b-20 hidden-xs hidden-sm`}
            style={{
            borderBottom: t.id === current.id ? '1px solid rgba(0, 0, 0, 0.25)' : ''
            }}>
            <span className="col-xs-12">{t.name}</span>
            <span className="team-position col-xs-12">{t.position}</span>
            </div>
            </div>
            ))}
            <div className="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 m-b-10 text-center team-name m-t-40 hidden-lg hidden-md">
            <span className="col-xs-12">{current.name}</span>
            <span className="team-position col-xs-12">{current.position}</span>
            </div>
            <div className="col-xs-12 col-md-8 col-md-offset-2 m-t-10">
            <div className="col-xs-12">
            <div className="col-xs-12 text-center">
            {current.socialMedia.map((media, index) =>
            <a
            key={index}
            className="team-icon"
            href={media.link}
            target="_blank">
            <i className={`fa ${media.icon} 2x`}></i>
            </a>
            )}
            </div>
            <div className="col-xs-12 m-t-20 m-b-20 team-divider"></div>
            <div className="col-xs-12 team-description">
            {current.description}
            </div>
            <div className="col-xs-12 team-stack m-t-20">{current.stack}</div>
            <div className="col-xs-12 m-t-40 m-b-40">
            <Link to="contact" spy={true} smooth={true} offset={0} duration={1000}>
            <button type="button" className="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 btn btn-success hire-button">
            hire us
            </button>
            </Link>
            </div>
            </div>
            </div>
            </div>
            </Element> */}
      </div>
    );
  }
}
