// @flow
import React from 'react';
import { Link } from 'react-scroll';

import logo from '../images/logo.png';

const Navigation = () => {
  const items = [['sekcje', 'col-xs-offset-8'], ['kontakt', '']];

  const navItems = items => items.map(([name, additionalClass]) => (
    <Link
      key={name}
      to={name}
      spy={true}
      smooth={true}
      offset={-60}
      duration={500}
      className={`${additionalClass} col-xs-2 nav-item text-right`}
    >
      {name}
    </Link>
  ));

  return (
    <div className="navigation">
      <div className="col-xs-2">
        <img
          src={logo}
          alt="logo"
          className="img-responsive col-xs-4 nav-logo"
        />
      </div>
      <div className="hidden-xs hidden-sm col-xs-9 text-right">
        {navItems(items)}
      </div>
    </div>
  );
};

export default Navigation;
