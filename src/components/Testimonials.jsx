// @flow
import React from 'react';

const Testimonials = ({ testimonials }) => {

  const settings = {
    dots: true,
    autoplay: true,
    autoplaySpeed: 12000,
    pauseOnHover: true,
    arrows: false
  };

  return (
    <div className="container-full section" style={{backgroundColor: 'white', color: 'black'}}>
      <div className="container">
        <h3 className="col-xs-12 text-center m-t-40 m-b-10">What people say</h3>
      </div>
      <div className="container m-b-40">
        {/* <Slider {...settings}>
            {testimonials.map((t, index) => (
            <div key={index} className="m-t-20 m-b-20">
            <div className="col-xs-12">
            <div className="col-xs-12 col-sm-10 col-sm-offset-1 testimonial-quote">
            {t.quote}
            </div>
            <div className="divider col-xs-10 col-xs-offset-1 m-t-20 m-b-20"></div>
            <div className="col-xs-12 col-sm-10 col-sm-offset-1 testimonial-person">
            <span className="col-xs-12 t-person-name">{t.name}</span>
            <span className="col-xs-12 t-person-who">{t.who}</span>
            </div>
            </div>
            </div>
            ))}
            </Slider> */}
      </div>
    </div>
  );

}

export default Testimonials
