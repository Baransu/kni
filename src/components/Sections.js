// @flow
import React from 'react';
import { Element } from 'react-scroll';

import Slider from 'react-slick';

import '../../node_modules/slick-carousel/slick/slick.css';
import '../../node_modules/slick-carousel/slick/slick-theme.css';

const settings = {
  dots: true,
  autoplay: true,
  autoplaySpeed: 12000,
  pauseOnHover: false,
  arrows: false,
};

const Sections = props => {
  const height = window.innerHeight;

  return (
    <Element name="sekcje" style={{ height: `${height * 9 / 10}px` }}>
      <Slider {...settings} className="m-b-40">
        {props.sections.map((section, index) => (
          <Element
            key={index}
            name={section.name}
            className="section-background col-xs-12"
            style={{
              height: `${height * 8.5 / 10}px`,
              background: `url(${section.image})`,
            }}
          >
            <div className="col-md-offset-5 col-md-6 col-xs-12 section-text">
              <h3 className="m-b-20">
                {section.name}
              </h3>
              <p className="section-description">
                {section.description}
              </p>
              {section.links.map((l, i) => (
                <a className="section-link" target="_blank" key={i} href={l}>
                  {l}
                </a>
              ))}
              <p className="stack">
                {`Technologie: ${section.stack.join(', ')}`}
              </p>
            </div>
          </Element>
        ))}
      </Slider>
    </Element>
  );
};

export default Sections;
