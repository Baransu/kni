// @flow
import React from 'react';
import { Element, Link } from 'react-scroll';

const Description = ({ description }) => {
  const height = window.innerHeight;
  return (
    <Element
      name="description"
      className="description container"
      style={{ height, paddingTop: '40px' }}
    >
      <h2 className="m-b-40">Kolo Naukowe Informatykow</h2>
      {description.map((d, i) => <p className="col-xs-12 m-b-30">{d}</p>)}
      <Link
        to="sekcje"
        spy={true}
        smooth={true}
        offset={-60}
        duration={1000}
        className="col-xs-12"
      >
        <button
          type="button"
          className="btn-lg col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 btn action-button col-md-2 col-md-offset-5 m-t-20"
        >
          sekcje kola
        </button>
      </Link>
    </Element>
  );
};

export default Description;
