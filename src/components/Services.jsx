// @flow
import React from 'react'

const Services = ({services, skills}) => {

  return (
    <div className="conteiner-full teaser-section section">
      <div className="container m-b-10">
        <h3 className="col-xs-12 m-t-10 m-b-20">Our services</h3>
        {services.map((c, index) => (
           <div key={index} className="col-xs-12 col-sm-4 teaser-card m-b-20">
             <div className="col-xs-12 t-card">
               <div className="col-xs-12 m-t-10" style={{color: c.color}}>
                 <i className={`fa ${c.icon} fa-5x`}></i>
               </div>
               <h4 className="col-xs-12">
                 {c.header}
               </h4>
               <div className="col-xs-12 m-b-10 t-card-description">
                 <span>
                   {c.text}
                 </span>
               </div>
             </div>
           </div>
         ))}
           <div className="divider m-b-10 m-t-20 col-xs-8 col-xs-offset-2"></div>
           <div className="col-xs-12">
             <h3 className="col-xs-12 m-t-10 m-b-20">We make these things easy</h3>
             {skills.map((s, index) => (
                <div key={index} className="col-xs-6 col-sm-4 col-md-3 col-lg-2 skill">
                  <img src={s.img} alt={s.name} className="img-responsive img-circle"/>
                  <span>{s.name}</span>
                </div>
              ))}
           </div>
      </div>
    </div>
  );
}

export default Services;

