// @flow
import React from 'react';
import { Link } from 'react-scroll';

const Header = ({ header, subHeader, description, callToAction }) => {
  const height = window.innerHeight;
  const width = window.innerWidth;

  return (
    <div className="head-section" style={{ height }}>
      <div
        className="col-xs-9 col-xs-offset-3 head-text"
        style={{
          marginTop: (width < 768 ? 15 : 35) * height / 100,
        }}
      >
        <div>{header}</div>
        <div className="m-t-20">
          {subHeader.map((s, i) => <div key={i}>{s}</div>)}
        </div>
        <Link
          to="description"
          spy={true}
          smooth={true}
          offset={-80}
          duration={1000}
          className="col-xs-12"
        >
          <button
            type="button"
            className="btn-lg col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 btn action-button col-md-2 col-md-offset-5 m-t-40"
          >
            {callToAction}
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Header;
