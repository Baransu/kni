// @flow
import React from 'react'

const Quality = ({ paragraphs }) => {

  return (
    <div className="container-full section m-t-30">
      <div className="container m-b-40">
        <h3 className="col-xs-12 m-t-20 text-center">Quality</h3>
        {paragraphs.map((p, index) => (
           <div key={index} className="col-xs-12">
             <span className="col-xs-10 col-xs-offset-1 quality-description m-t-40 m-b-40">
               {p}
             </span>
             {index !== paragraphs.length - 1 ? (
                <div className="divider col-xs-6 col-xs-offset-3"></div>
              ) : ('')}
           </div>
         ))}
      </div>
    </div>
  );
}

export default Quality;
