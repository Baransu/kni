// @flow
import React from 'react';
import { Element } from 'react-scroll';

const Footer = ({ contact }) => {
  const date = new Date();

  const Text = ({ type, text }) => {
    if (type === 'tel') {
      return <a href={`tel:${text}`} className="col-xs-12">{text}</a>;
    }

    if (type === 'mailto') {
      return <a href={`mailto:${text}`} className="col-xs-12">{text}</a>;
    }

    return <span className="col-xs-12">{text}</span>;
  };

  return (
    <Element name="kontakt" className="col-xs-12">

      <div className="container">
        <h3 className="col-xs-12 text-center m-t-40 m-b-10">Kontakt</h3>
        {contact.map((c, index) => (
          <div key={index} className="col-xs-12 col-sm-4 contact-card m-b-20">
            <div className="col-xs-12 t-card">
              <div className="col-xs-12 m-t-10">
                <i className={`fa ${c.icon} fa-2x`} />
              </div>
              <h4 className="col-xs-12">
                {c.header}
              </h4>
              <div className="col-xs-12 t-card-description">
                {c.text.map((t, index) => (
                  <Text key={index} text={t} type={c.type} />
                ))}
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="col-xs-12 m-b-40 m-t-20">
        <h3>Napisz do nas</h3>
        <a
          href="mailto:example@example.com"
          type="button"
          className="btn-lg col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 btn action-button col-md-4 col-md-offset-4 m-t-20"
        >
          {'example@example.com'}
        </a>
      </div>
      <div className="text-center m-b-40 m-t-40 col-xs-12">
        Copyright © {date.getFullYear()} Kolo Naukowe Informatykow PK
      </div>
    </Element>
  );
};

export default Footer;
