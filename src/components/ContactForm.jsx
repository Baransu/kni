// @flow
import React, { Component } from 'react';

export default class ContactForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
      subject: '',
      href: 'mailto:contact@neontree.pl'
    }
  }

  encode({ message, subject }) {
    return {
      message: message,
      subject: subject
    };
  }

  onChange(prop, value) {
    this.setState({ [prop]: value });
    const { message, subject } = this.encode(this.state);
    this.setState({
      href: `mailto:contact@neontree.pl?subject=${subject}&body=${message}`
    });
  }

  render() {

    const ErrorText = ({ text = "" }) => (
      <p className="col-xs-12 text-danger">{text}</p>
    );

    const { message, subject, href } = this.state;
    let messageInput, subjectInput;

    return (
      <div className="container-full contact-form-section">
        <div className="container">
          <h3 className="col-xs-12 text-center m-t-40">Write to us</h3>
          <div className="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 m-t-20 m-b-30 form-group">

            <div className="col-xs-12">
              <input
                className="form-control"
                placeholder="Subject"
                value={subject}
                ref={(node) => subjectInput = node }
                onChange={() => this.onChange('subject', subjectInput.value)}
              />
              <ErrorText text=""/>
            </div>

            <div className="col-xs-12">
              <textarea
                placeholder="Your message"
                className="form-control"
                value={message}
                ref={(node) => messageInput = node }
                onChange={() => this.onChange('message', messageInput.value)}
              />
            </div>

            <a href={href}
              className="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 m-t-20 btn btn-success hire-button">
              send message
            </a>

          </div>
        </div>
      </div>
    );
  }
}
