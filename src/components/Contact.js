// @flow
import React from 'react';
import { Element } from 'react-scroll';

const Contact = ({ contacts = [] }) => {
  const Text = ({ type, text }) => {
    if (type === 'tel')
      return <a href={`tel:${text}`} className="col-xs-12">{text}</a>;

    if (type === 'mailto')
      return <a href={`mailto:${text}`} className="col-xs-12">{text}</a>;

    return <span className="col-xs-12">{text}</span>;
  };

  return (
    <Element
      name="kontakt"
      className="section text-center contact-section container-full"
    >
      <h3 className="col-xs-12 text-center m-t-40 m-b-10">Contact</h3>
      {contacts.map((c, index) => (
        <div key={index} className="col-xs-12 col-sm-4 contact-card m-b-20">
          <div className="col-xs-12 t-card">
            <div className="col-xs-12 m-t-10">
              <i className={`fa ${c.icon} fa-2x`} />
            </div>
            <h4 className="col-xs-12">
              {c.header}
            </h4>
            <div className="col-xs-12 t-card-description">
              {c.text.map((t, index) => (
                <Text key={index} text={t} type={c.type} />
              ))}
            </div>
          </div>
        </div>
      ))}
    </Element>
  );
};

export default Contact;
