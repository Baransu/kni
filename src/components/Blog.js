// @flow
import React, { Component } from 'react';
import axios from 'axios';
import X2JS from 'x2js';

class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      posts: []
    }
  }

  componentDidMount() {
    // make request to blog.neontree.pl/feed.xml and save to state
    axios.get('http://blog.neontree.pl/feed.xml')
         .then(response => {
           const parser = new X2JS();
           const json = parser.xml2js(response.data);
           this.setState({
             posts: [
               ...json.rss.channel.item.slice(0, 3)
             ]
           });
         })
         .catch(err => this.setState({ error: true }))
  }

  render() {

    const { posts, error } = this.state;

    const Loader = () => {
      if(error) {
        return (
          <span className="col-xs-12 blog-error m-b-20">
            Could not load latest posts
          </span>
        )
      }

      if(posts.length === 0) {
        return (
          <div className="col-xs-12">
            <div className="spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </div>
        )
      }

      return null
    }

    const Post = ({ title, link, meta, author }) => {
      const months = [
        "January", "February", " March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"
      ]

      const d = new Date(meta);

      const date = `${months[d.getMonth()]} ${d.getDate()}, ${d.getFullYear()}`

      return (
        <div className="col-xs-12 col-sm-4 m-b-30 post">
          <span className="col-xs-12">{author}</span>
          <span className="col-xs-12 post-meta">
            {date}
          </span>
          <span className="col-xs-12 divider m-t-10 m-b-10"></span>
          <a href={link} className="col-xs-12 post-title m-b-10">
            {title}
          </a>
        </div>
      )
    }

    return (
      <div className="container-full section blog-section">
        <div className="container text-center">

          <h3 className="col-xs-12 m-t-40 m-b-40">
            Latest posts on blog
          </h3>

          <div className="col-xs-12">
            tutaj beda ostatnie 3 naglowki z bloga
          </div>

        </div>
      </div>
    )
  }
}

export default Blog;
