// @flow
import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';

import Layout from './pages/Layout';
import Index from './pages/Index';

const App = () => {
  return (
    <Router history={browserHistory}>
      <Route path="*" component={Layout}>
        <IndexRoute component={Index} />
      </Route>
    </Router>
  );
};

export default App;
