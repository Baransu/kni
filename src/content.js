// @flow

import image from './images/bg_1.jpg';
import androidImage from './images/android_bg.jpeg';

const content = {
  description: [
    `Nasze Koło prowadzi współpracę z wieloma kołami informatycznymi w Polsce. Głównie zaś współpracujemy z Kołem Informatyki Bit AGH, Kołem Naukowym Informatyki UEk oraz Kołem Studentów Informatyki UJ przy organizacji największej studenckiej konferencji informatycznej w Polsce - Studenckiego Festiwalu Informatycznego.`,
    `Aktualnie KNI to grupa kilkunastu osób dla których informatyka jest największą pasją. Wspólnie realizujemy ambitne projekty dbając o najwyższą jakość tworzonych rozwiązań. Staramy się, aby działalność w Kole dobrze przygotowywała studentów do przyszłej kariery - stosujemy sprawdzone metodyki prowadzenia projektów, dbamy o jakość procesu wytwarzania oprogramowania, kładziemy duży nacisk na testowanie aplikacji. Korzystamy również z profesjonalnych narzędzi wpierającego różne aspekty projektów.`,
  ],
  head: {
    header: 'Kolo Naukowe Informatykow\n',
    subHeader: ['Politechniki Krakowskiej', 'im. Tadeusza Kosciuszki'],
    callToAction: 'Wiecej',
  },
  sections: [
    {
      name: 'android',
      image: androidImage,
      links: ['http://credo.science'],
      stack: ['Java', 'Anroid'],
      description: (
        `Aktualnie sekcja "Android" nawiązała współpracę z krakowskim Instytutem Fizyki Jądrowej w celu pomocy przy tworzeniu aplikacji mobilnej dla projektu CREDO. Projekt ma na celu zdobywanie informacji o ciemnej materii poprzez analizę promieniowania kosmicznego. Aplikacja umożliwi ludziom z całego świata wzięcie udziału w zdobywaniu próbek promieniowania bez specjalistycznej wiedzy. Więcej informacji o projekcie na stronie projektu.`
      ),
    },
    {
      name: 'security',
      links: [],
      image,
      stack: [],
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur laoreet diam justo, sit amet finibus nisi faucibus vel. Integer consectetur arcu sit amet magna suscipit pellentesque. Ut fermentum nisi sit amet mattis vehicula. Suspendisse vitae viverra quam..',
    },
    {
      name: 'web',
      links: [],
      image,
      stack: ['Elixir', 'Elm', 'React'],
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur laoreet diam justo, sit amet finibus nisi faucibus vel. Integer consectetur arcu sit amet magna suscipit pellentesque. Ut fermentum nisi sit amet mattis vehicula. Suspendisse vitae viverra quam.',
    },
    {
      name: 'game dev',
      image,
      links: [],
      stack: ['C++', 'SFML'],
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur laoreet diam justo, sit amet finibus nisi faucibus vel. Integer consectetur arcu sit amet magna suscipit pellentesque. Ut fermentum nisi sit amet mattis vehicula. Suspendisse vitae viverra quam..',
    },
  ],
  contact: [
    {
      header: 'Adres',
      icon: 'fa-map-marker',
      text: ['gdzies tam', 'krakow', 'polsha'],
    },
    {
      header: 'E-mail',
      icon: 'fa-envelope-o',
      type: 'mailto',
      text: ['example@example.pl'],
    },
    {
      header: 'Telefon',
      type: 'tel',
      icon: 'fa-phone',
      text: ['+048 666 666 666'],
    },
  ],
};

export default content;
